env=$1

#pull down updated site content
rm -rf sites
git clone git@gitlab.com:denis-singh/isentia-content.git sites

#login in AWS ECR
aws ecr get-login --region ap-southeast-2 --no-include-email | bash -

##Build docker nginx image and push to ECR
docker build -t 605767546133.dkr.ecr.ap-southeast-2.amazonaws.com/isentia-web:${BUILD_NUMBER} .
docker push 605767546133.dkr.ecr.ap-southeast-2.amazonaws.com/isentia-web:${BUILD_NUMBER}

#Deploy to ECS using ansible
git submodule add git@gitlab.com:denis-singh/isentia-ansible.git ansible
cd ansible
ansible-playbook -vvv -i localhost deploy_infra_app_stack.yml -e "docker_tag=${BUILD_NUMBER} app_stack=isentia_web env=${env} create_or_destroy=destroy"
