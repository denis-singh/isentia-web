FROM docker.io/nginx:latest

MAINTAINER Denis Singh <singh.denis@gmail.com>

RUN rm -f /etc/nginx/conf.d/default.conf
RUN rm -f /etc/nginx/nginx.conf
RUN mkdir -p /etc/nginx/ssl
RUN mkdir -p /etc/nginx/sites-enabled
RUN mkdir -p /var/www/html/isentia

COPY nginx.conf /etc/nginx/
COPY conf.d/* /etc/nginx/conf.d/
COPY sites/isentia-web/public/ /var/www/html/isentia/

RUN chown -R nginx /var/www/html/isentia/
